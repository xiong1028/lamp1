<?php
/**
 * Purpose: Build a function to display the selected record
 * Authors: Xiong Li, Haiyun Xiao, Kunj Bhavsar,Nayan, Goswami
 *
 */

function displayRecord($selectedId)
{
    $db_conn = dbconnect('localhost', 'week7', 'lamp1user', '!Lamp1!');
    $ct_type = "";
    $ct_first_name = "";
    $ct_last_name = "";
    $ct_disp_name = "";
    $ad_type = "";
    $ad_line_1 = "";
    $ad_line_2 = "";
    $ad_line_3 = "";
    $ad_city = "";
    $ad_province = "";
    $ad_post_code = "";
    $ad_country = "";
    $ph_type = "";
    $ph_number = "";
    $em_type = "";
    $em_email = "";
    $we_type = "";
    $we_url = "";
    $no_note ="";

    if ($rs = $db_conn->query("select ct_type, ct_first_name,ct_last_name,ct_disp_name from contact where ct_id = " . $selectedId . ";")) {
        if ($rs->num_rows > 0) {
            $row = $rs->fetch_assoc();
            $ct_type = $row['ct_type'];
            $ct_first_name = $row['ct_first_name'];
            $ct_last_name = $row['ct_last_name'];
            $ct_disp_name = $row['ct_disp_name'];
        }
    }
    if ($rs = $db_conn->query("select ad_type, ad_line_1, ad_line_2, ad_line_3,ad_city,ad_province,ad_post_code,ad_country from contact_address where ad_ct_id = " . $selectedId . ";")) {
        if ($rs->num_rows > 0) {
            $row = $rs->fetch_assoc();
            $ad_type = $row['ad_type'];
            $ad_line_1 = $row['ad_line_1'];
            $ad_line_2 = $row['ad_line_2'];
            $ad_line_3 = $row['ad_line_3'];
            $ad_city = $row['ad_city'];
            $ad_post_code = $row['ad_post_code'];
            $ad_country = $row['ad_country'];
        }
    }

    if ($rs = $db_conn->query("select ph_type, ph_number from contact_phone where ph_ct_id = " . $selectedId . ";")) {
        if ($rs->num_rows > 0) {
            $row = $rs->fetch_assoc();
            $ph_type = $row['ph_type'];
            $ph_number = $row['ph_number'];
        }
    }

    if ($rs = $db_conn->query("select em_type, em_email from contact_email where em_ct_id = " . $selectedId . ";")) {
        if ($rs->num_rows > 0) {
            $row = $rs->fetch_assoc();
            $em_type = $row['em_type'];
            $em_email = $row['em_email'];
        }
    }

    if ($rs = $db_conn->query("select we_type,we_url from contact_web where we_ct_id = " . $selectedId . ";")) {
        if ($rs->num_rows > 0) {
            $row = $rs->fetch_assoc();
            $we_type = $row['we_type'];
            $we_url = $row['we_url'];
        }
    }

    if ($rs = $db_conn->query("select no_note from contact_note where no_ct_id = " . $selectedId . ";")) {
        if ($rs->num_rows > 0) {
            $row = $rs->fetch_assoc();
            $no_note = $row['no_note'];
        }
    }

    dbdisconnect($db_conn);
    ?>
    <h3>Display Record </h3>
    <form method="POST">
        <table>
            <tr>
                <td>Contact Type</td>
                <td><?php echo $ct_type; ?></td>
            </tr>
            <tr>
                <td>Display/Business Name</td>
                <td><?php echo $ct_disp_name; ?></td>
            </tr>
            <tr>
                <td>First Name</td>
                <td><?php echo $ct_first_name; ?></td>
            </tr>
            <tr>
                <td>Last Name</td>
                <td><?php echo $ct_last_name; ?></td>
            </tr>
            <tr>
                <td><br></td>
                <td></td>
            </tr>
            <tr>
                <td>Address Type</td>
                <td><?php echo $ad_type; ?></td>
            </tr>
            <tr>
                <td>Address Line 1</td>
                <td><?php echo $ad_line_1; ?></td>
            </tr>
            <tr>
                <td>Address Line 2</td>
                <td><?php echo $ad_line_2; ?></td>
            </tr>
            <tr>
                <td>Address Line 3</td>
                <td><?php echo $ad_line_3; ?></td>
            </tr>
            <tr>
                <td>City</td>
                <td><?php echo $ad_city; ?></td>
            </tr>
            <tr>
                <td>Province</td>
                <td><?php echo $ad_province; ?></td>
            </tr>
            <tr>
                <td>Post Code</td>
                <td><?php echo $ad_post_code; ?></td>
            </tr>
            <tr>
                <td>Country</td>
                <td><?php echo $ad_country; ?></td>
            </tr>
            <tr>
                <td><br></td>
                <td></td>
            </tr>
            <tr>
                <td>Phone Type</td>
                <td><?php echo $ph_type; ?></td>
            </tr>
            <tr>
                <td>Phone Number</td>
                <td><?php echo $ph_number; ?></td>
            </tr>
            <tr>
                <td><br></td>
                <td></td>
            </tr>
            <tr>
                <td>Email Type</td>
                <td><?php echo $em_type; ?></td>
            </tr>

            <tr>
                <td>Email Address</td>
                <td><?php echo $em_email; ?></td>
            </tr>
            <tr>
                <td><br></td>
                <td></td>
            </tr>
            <tr>
                <td>Web Site Type</td>
                <td><?php echo $we_type; ?></td>
            </tr>
            <tr>
                <td>Web Site URL</td>
                <td><?php echo $we_url; ?></td>
            </tr>
            <tr>
                <td><br></td>
                <td></td>
            </tr>
            <tr>
                <td>Note</td>
                <td><?php echo $no_note; ?></td>
            </tr>
        </table>
        <input type="submit" name="ct_b_OK" value="OK">
    </form>


    <?php
}

?>






