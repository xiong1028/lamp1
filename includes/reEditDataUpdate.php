<?php
/**
 * Purpose: update the re-Edited data in the databases
 * Authors: Xiong Li, Haiyun Xiao, Kunj Bhavsar,Nayan, Goswami
 *
 **/
function reEditDataUpdate($selectId)
{
    $db_conn = dbconnect('localhost', 'week7', 'lamp1user', '!Lamp1!');
    updateContact($db_conn, $selectId);
    updateContactAddress($db_conn, $selectId);
    updateContactPhone($db_conn, $selectId);
    updateContactEmail($db_conn, $selectId);
    updateContactWeb($db_conn, $selectId);
    updateContactNote($db_conn, $selectId);

    dbdisconnect($db_conn);
}

?>

<?php
//update data in table contact
function updateContact($db_conn, $selectId)
{
    $isExisted = "";
    if ($rs = $db_conn->query("SELECT count(*) from contact WHERE ct_id =" . $selectId . ";")) {
        if ($row = $rs->fetch_assoc()) {
            if ($row['count(*)'] == 1) {
                $isExisted = true;
            } else {
                $isExisted = false;
            }
        }
    }


    if ($isExisted) {
        $qry_ct_update = "update contact set ct_type='" . $db_conn->real_escape_string($_SESSION['ct_type']) . "'";
        if (isset($_SESSION['ct_first_name'])) {
            $qry_ct_update .= ", ct_first_name='" . $db_conn->real_escape_string($_SESSION['ct_first_name']) . "'";
        } else {
            $qry_ct_update .= ", ct_first_name=''";
        }
        if (isset($_SESSION['ct_last_name'])) {
            $qry_ct_update .= ", ct_last_name='" . $db_conn->real_escape_string($_SESSION['ct_last_name']) . "'";
        } else {
            $qry_ct_update .= ", ct_last_name=''";
        }
        if (isset($_SESSION['ct_disp_name'])) {
            $qry_ct_update .= ", ct_disp_name='" . $db_conn->real_escape_string($_SESSION['ct_disp_name']) . "'";
        } else {
            $qry_ct_update .= ", ct_disp_name=''";
        }
        $qry_ct_update .= " where ct_id = " . $selectId . ";";

        $db_conn->query($qry_ct_update);
    } else {
        $qry_ct = "insert into contact set ct_type='" . $db_conn->real_escape_string($_SESSION['ct_type']) . "'";
        if (isset($_SESSION['ct_first_name'])) {
            $qry_ct .= ", ct_first_name='" . $db_conn->real_escape_string($_SESSION['ct_first_name']) . "'";
        } else {
            $qry_ct .= ", ct_first_name=''";
        }
        if (isset($_SESSION['ct_last_name'])) {
            $qry_ct .= ", ct_last_name='" . $db_conn->real_escape_string($_SESSION['ct_last_name']) . "'";
        } else {
            $qry_ct .= ", ct_last_name=''";
        }
        if (isset($_SESSION['ct_disp_name'])) {
            $qry_ct .= ", ct_disp_name='" . $db_conn->real_escape_string($_SESSION['ct_disp_name']) . "'";
        } else {
            $qry_ct .= ", ct_disp_name=''";
        }
        $qry_ct .= ", ct_deleted='N';";

        $db_conn->query($qry_ct);
    }
}

?>

<?php
//update contact_address table
function updateContactAddress($db_conn, $selectId)
{
    $isExisted = "";
    if ($rs = $db_conn->query("SELECT count(*) from contact_address WHERE ad_ct_id =" . $selectId . ";")) {
        if ($row = $rs->fetch_assoc()) {
            if ($row['count(*)'] == 1) {
                $isExisted = true;
            } else {
                $isExisted = false;
            }
        }
    }

    if ($isExisted) {
        $qry_ad_update = "update contact_address set ad_type='" . $db_conn->real_escape_string($_SESSION['ad_type']) . "'";
        if (isset($_SESSION['ad_line_1'])) {
            $qry_ad_update .= ", ad_line_1='" . $db_conn->real_escape_string($_SESSION['ad_line_1']) . "'";
        } else {
            $qry_ad_update .= ", ad_line_1 =''";
        }
        if (isset($_SESSION['ad_line_2'])) {
            $qry_ad_update .= ", ad_line_2='" . $db_conn->real_escape_string($_SESSION['ad_line_2']) . "'";
        } else {
            $qry_ad_update .= ", ad_line_2=''";
        }
        if (isset($_SESSION['ad_line_3'])) {
            $qry_ad_update .= ", ad_line_3='" . $db_conn->real_escape_string($_SESSION['ad_line_3']) . "'";
        } else {
            $qry_ad_update .= ", ad_line_3=''";
        }
        if (isset($_SESSION['ad_city'])) {
            $qry_ad_update .= ", ad_city='" . $db_conn->real_escape_string($_SESSION['ad_city']) . "'";
        } else {
            $qry_ad_update .= ", ad_city=''";
        }
        if (isset($_SESSION['ad_province'])) {
            $qry_ad_update .= ", ad_province='" . $db_conn->real_escape_string($_SESSION['ad_province']) . "'";
        } else {
            $qry_ad_update .= ", ad_province=''";
        }
        if (isset($_SESSION['ad_post_code'])) {
            $qry_ad_update .= ", ad_post_code='" . $db_conn->real_escape_string($_SESSION['ad_post_code']) . "'";
        } else {
            $qry_ad_update .= ", ad_post_code=''";
        }
        if (isset($_SESSION['ad_contry'])) {
            $qry_ad_update .= ", ad_country='" . $db_conn->real_escape_string($_SESSION['ad_country']) . "'";
        } else {
            $qry_ad_update .= ", ad_country=''";
        }
        $qry_ad_update .= " where ad_ct_id = " . $selectId . ";";

        $db_conn->query($qry_ad_update);

    } else {
        $qry_ad = "insert into contact_address set ad_ct_id='" . $selectId . "'";

        if (isset($_SESSION['ad_type'])) {
            $qry_ad .= ", ad_type='" . $db_conn->real_escape_string($_SESSION['ad_type']) . "'";
            if (isset($_SESSION['ad_line_1'])) {
                $qry_ad .= ", ad_line_1='" . $db_conn->real_escape_string($_SESSION['ad_line_1']) . "'";
            } else {
                $qry_ad .= ", ad_line_1=''";
            }
            if (isset($_SESSION['ad_line_2'])) {
                $qry_ad .= ", ad_line_2='" . $db_conn->real_escape_string($_SESSION['ad_line_2']) . "'";
            } else {
                $qry_ad .= ", ad_line_2=''";
            }
            if (isset($_SESSION['ad_line_3'])) {
                $qry_ad .= ", ad_line_3='" . $db_conn->real_escape_string($_SESSION['ad_line_3']) . "'";
            } else {
                $qry_ad .= ", ad_line_3=''";
            }
            if (isset($_SESSION['ad_city'])) {
                $qry_ad .= ", ad_city='" . $db_conn->real_escape_string($_SESSION['ad_city']) . "'";
            } else {
                $qry_ad .= ", ad_city=''";
            }
            if (isset($_SESSION['ad_province'])) {
                $qry_ad .= ", ad_province='" . $db_conn->real_escape_string($_SESSION['ad_province']) . "'";
            } else {
                $qry_ad .= ", ad_province=''";
            }
            if (isset($_SESSION['ad_post_code'])) {
                $qry_ad .= ", ad_post_code='" . $db_conn->real_escape_string($_SESSION['ad_post_code']) . "'";
            } else {
                $qry_ad .= ", ad_post_code=''";
            }
            if (isset($_SESSION['ad_contry'])) {
                $qry_ad .= ", ad_country='" . $db_conn->real_escape_string($_SESSION['ad_country']) . "'";
            } else {
                $qry_ad .= ", ad_country=''";
            }
            $qry_ad .= ", ad_active='Y';";
        }

        $db_conn->query($qry_ad);
    }
}

?>

<?php
//update contact_phone table
function updateContactPhone($db_conn, $selectId)
{
    $isExisted = "";
    if ($rs = $db_conn->query("SELECT count(*) from contact_phone WHERE ph_ct_id =" . $selectId . ";")) {
        if ($row = $rs->fetch_assoc()) {
            if ($row['count(*)'] == 1) {
                $isExisted = true;
            } else {
                $isExisted = false;
            }
        }
    }

    if ($isExisted) {
        $qry_ph_update = "update contact_phone set ph_type = '" . $db_conn->real_escape_string($_SESSION['ph_type']) . "'";
        if (isset($_SESSION['ph_number'])) {
            $qry_ph_update .= ", ph_number ='" . $db_conn->real_escape_string($_SESSION['ph_number']) . "'";
        } else {
            $qry_ph_update .= ", ph_number =''";
        }
        $qry_ph_update .= " where ph_ct_id = " . $selectId . ";";

        $db_conn->query($qry_ph_update);

    } else {
        $qry_ph = "insert into contact_phone set ph_ct_id='" . $selectId . "'";
        $qry_ph .= ", ph_type='" . $db_conn->real_escape_string($_SESSION['ph_type']) . "'";
        if (isset($_SESSION['ph_number'])) {
            $qry_ph .= ", ph_number='" . $db_conn->real_escape_string($_SESSION['ph_number']) . "'";
        } else {
            $qry_ph .= ", ph_number=''";
        }
        $qry_ph .= ", ph_active='Y';";

        $db_conn->query($qry_ph);
    }
}

?>

<?php
//update contact_email table
function updateContactEmail($db_conn, $selectId)
{
    $isExisted = "";
    if ($rs = $db_conn->query("SELECT count(*) from contact_email WHERE em_ct_id =" . $selectId . ";")) {
        if ($row = $rs->fetch_assoc()) {
            if ($row['count(*)'] == 1) {
                $isExisted = true;
            } else {
                $isExisted = false;
            }
        }
    }

    if ($isExisted) {
        $qry_em_update = "update contact_email set em_type = '" . $db_conn->real_escape_string($_SESSION['em_type']) . "'";
        if (isset($_SESSION['em_email'])) {
            $qry_em_update .= ", em_email ='" . $db_conn->real_escape_string($_SESSION['em_email']) . "'";
        } else {
            $qry_em_update .= ", em_email =''";
        }
        $qry_em_update .= " where em_ct_id = " . $selectId . ";";

        $db_conn->query($qry_em_update);

    } else {
        $qry_em = "insert into contact_email set em_ct_id='" . $selectId . "'";
        $qry_em .= ", em_type='" . $db_conn->real_escape_string($_SESSION['em_type']) . "'";
        if (isset($_SESSION['em_email'])) {
            $qry_em .= ", em_email='" . $db_conn->real_escape_string($_SESSION['em_email']) . "'";
        } else {
            $qry_em .= ", em_email =''";
        }
        $qry_em .= ", ph_active='Y';";

        $db_conn->query($qry_em);
    }
}

?>

<?php
//update contact_web table
function updateContactWeb($db_conn, $selectId)
{
    $isExisted = "";
    if ($rs = $db_conn->query("SELECT count(*) from contact_web WHERE we_ct_id =" . $selectId . ";")) {
        if ($row = $rs->fetch_assoc()) {
            if ($row['count(*)'] == 1) {
                $isExisted = true;
            } else {
                $isExisted = false;
            }
        }
    }

    if ($isExisted) {
        $qry_we_update = "update contact_web set we_type = '" . $db_conn->real_escape_string($_SESSION['we_type']) . "'";
        if (isset($_SESSION['we_url'])) {
            $qry_we_update .= ", we_url ='" . $db_conn->real_escape_string($_SESSION['we_url']) . "'";
        } else {
            $qry_we_update .= ", we_url =''";
        }
        $qry_we_update .= " where we_ct_id = " . $selectId . ";";

        $db_conn->query($qry_we_update);

    } else {
        $qry_we = "insert into contact_web  set we_ct_id='" . $selectId . "'";
        $qry_we .= ", we_type='" . $db_conn->real_escape_string($_SESSION['we_type']) . "'";
        if (isset($_SESSION['we_url'])) {
            $qry_we .= ", we_url='" . $db_conn->real_escape_string($_SESSION['we_url']) . "'";
        } else {
            $qry_we .= ", we_url=''";
        }
        $qry_we .= ", we_active='Y';";

        $db_conn->query($qry_we);
    }
}

?>

<?php
//update contact_note table
function updateContactNote($db_conn, $selectId)
{
    $isExisted = "";
    if ($rs = $db_conn->query("SELECT count(*) from contact_note WHERE no_ct_id =" . $selectId . ";")) {
        if ($row = $rs->fetch_assoc()) {
            if ($row['count(*)'] == 1) {
                $isExisted = true;
            } else {
                $isExisted = false;
            }
        }
    }

    if ($isExisted) {
        $qry_no_update = "update contact_note set no_type =''";
        if (isset($_SESSION['no_note'])) {
            $qry_no_update .= ", no_note ='" . $db_conn->real_escape_string($_SESSION['no_note']) . "'";
        } else {
            $qry_no_update .= ", no_note =''";
        }
        $qry_no_update .= " where no_ct_id = " . $selectId . ";";



        $db_conn->query($qry_no_update);

    } else {
        $qry_no = "insert into contact_note  set no_ct_id='" . $selectId . "'";
        $qry_no .= ", no_type=''";
        $qry_no .= ", no_note='" . $db_conn->real_escape_string($_SESSION['no_note']) . "';";

        $db_conn->query($qry_no);
    }
}
?>


