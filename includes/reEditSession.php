<?php
/**
 * Purpose: Re-Edit the $_SESSION after edit the data
 * Authors: Xiong Li, Haiyun Xiao, Kunj Bhavsar,Nayan, Goswami
 *
 */
function reEditSession()
{
    $_SESSION['ct_id'] = isset($_POST['ct_id']) ? $_POST['ct_id'] : '';
    $_SESSION['ct_type'] = isset($_POST['ct_type']) ? trim($_POST['ct_type']) : '';
    $_SESSION['ct_first_name'] = isset($_POST['ct_first_name']) ? trim($_POST['ct_first_name']) : '';
    $_SESSION['ct_last_name'] = isset($_POST['ct_last_name']) ? trim($_POST['ct_last_name']) : '';

    $_SESSION['ct_disp_name']  = "";
    if(strlen($_POST['ct_disp_name'])==0){
        $_SESSION['ct_disp_name'] =trim($_POST['ct_first_name']).", ".trim($_POST['ct_last_name']);
    }else{
        $_SESSION['ct_disp_name'] = trim($_POST['ct_disp_name']);
    }
    $_SESSION['ad_type'] = isset($_POST['ad_type']) ? trim($_POST['ad_type']) : '';
    $_SESSION['ad_line_1'] = isset($_POST['ad_line_1']) ? trim($_POST['ad_line_1']) : '';
    $_SESSION['ad_line_2'] = isset($_POST['ad_line_2']) ? trim($_POST['ad_line_2']) : '';
    $_SESSION['ad_line_3'] = isset($_POST['ad_line_3']) ? trim($_POST['ad_line_3']) : '';
    $_SESSION['ad_city'] = isset($_POST['ad_city']) ? trim($_POST['ad_city']) : '';
    $_SESSION['ad_province'] = isset($_POST['ad_province']) ? trim($_POST['ad_province']) : '';
    $_SESSION['ad_post_code'] = isset($_POST['ad_post_code']) ? trim($_POST['ad_post_code']) : '';
    $_SESSION['ad_country'] = isset($_POST['ad_country']) ? trim($_POST['ad_country']) : '';
    $_SESSION['ph_type'] = isset($_POST['ph_type']) ? trim($_POST['ph_type']) : '';
    $_SESSION['ph_number'] = isset($_POST['ph_number']) ? trim($_POST['ph_number']) : '';
    $_SESSION['em_type'] = isset($_POST['em_type']) ? trim($_POST['em_type']) : '';
    $_SESSION['em_email'] = isset($_POST['em_email']) ? trim($_POST['em_email']) : '';
    $_SESSION['we_type'] = isset($_POST['we_type']) ? trim($_POST['we_type']) : '';
    $_SESSION['we_url'] = isset($_POST['we_url']) ? trim($_POST['we_url']) : '';
    $_SESSION['no_note'] = isset($_POST['no_note']) ? trim($_POST['no_note']) : '';
}

?>