<?php
/**
 * Purpose: Display the index entrance form
 * Authors: Xiong Li, Haiyun Xiao, Kunj Bhavsar,Nayan, Goswami
 *
 **/



function formContactDisplay(){
    $db_conn = dbconnect('localhost', 'week7', 'lamp1user', '!Lamp1!');
    $fvalue = "";
    if (isset($_POST['ct_b_filter']) && isset($_POST['ct_filter'])) {
        $_SESSION['ct_filter'] = $db_conn->real_escape_string(trim($_POST['ct_filter']));
        $fvalue = $_SESSION['ct_filter'];
    } else if (isset($_POST['ct_b_filter_clear'])) {
        $_SESSION['ct_filter'] = "";
        $fvalue = $_SESSION['ct_filter'];
    } else if (isset($_SESSION['ct_filter'])) {
        $fvalue = $_SESSION['ct_filter'];
    }
    ?>
    <h1> Contacts </h1>
    <div>
        <h2> Contacts </h2>
    </div>
    <div>
        <form method="POST">
            <table>
                <tr>
                    <td><label for="ct_filter">Filter Value</label></td>
                    <td><input type="text" name="ct_filter" id="ct_filter" value="<?php echo $fvalue; ?>"></td>
                    <td><input type="submit" name="ct_b_filter" value="Filter">
                    <td><input type="submit" name="ct_b_filter_clear" value="Clear Filter">
                </tr>
            </table>
            <br>
            <?php
            displayContacts($db_conn);
            dbdisconnect($db_conn);
            ?>
            <br>
            <table>
                <tr>
                    <td><input type="submit" name="ct_b_view" value="View Details"></td>
                    <td><input type="submit" name="ct_b_edit" value="Edit"></td>
                    <td><input type="submit" name="ct_b_delete" value="Delete"></td>
                </tr>
                <tr></tr>
                <tr>
                    <td><input type="submit" name="ct_b_add" value="Add New Contact"></td>
                </tr>
            </table>
        </form>
    </div>

    <?php
}

?>