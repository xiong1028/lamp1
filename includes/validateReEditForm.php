<?php
/**
 * Purpose: a function to validate the re-edit form
 * Authors: Xiong Li, Haiyun Xiao, Kunj Bhavsar,Nayan, Goswami
 *
 **/
function validateReEditForm()
{
    $err_msgs = array();
    //validate the names field
    if (isset($_POST['ct_type']) && $_POST['ct_type'] == "Business") {
        if (!isset($_POST['ct_disp_name'])) {
            $err_msgs['ct_disp_name'] = "A business name must be specified";
        } else {
            $dname = trim($_POST['ct_disp_name']);
            if (strlen($dname) == 0) {
                $err_msgs['ct_disp_name'] = "A business name must be specified";
            } else if (strlen($dname) > 200) {
                $err_msgs['ct_disp_name'] = "The business name must be no longer than 200 characters in length.";
            }
        }
        if (count($err_msgs) == 0) {
            $_SESSION['ct_disp_name'] = $dname;
            $_SESSION['ct_first_name'] = (isset($_POST['ct_first_name'])) ?
                trim($_POST['ct_first_name']) :
                "";
            $_SESSION['ct_last_name'] = (isset($_POST['ct_last_name'])) ?
                trim($_POST['ct_last_name']) :
                "";
        }
    } else {
        if (isset($_POST['ct_type']) && $_POST['ct_type'] == "Choice") {
            $err_msgs['ct_type'] = "The valid contact type must be selected.";
        }
        if (!isset($_POST['ct_first_name'])) {
            $err_msgs['ct_first_name'] = "A first name must be specified";
        } else {
            $fname = trim($_POST['ct_first_name']);
            if (strlen($fname) == 0) {
                $err_msgs['ct_first_name'] = "A first name must be specified";
            } else if (strlen($fname) > 100) {
                $err_msgs['ct_first_name'] = "The first name must be no longer than 100 characters in length.";
            }
        }
        if (!isset($_POST['ct_last_name'])) {
            $err_msgs['ct_last_name'] = "A last name must be specified";
        } else {
            $lname = trim($_POST['ct_last_name']);
            if (strlen($lname) == 0) {
                $err_msgs['ct_last_name'] = "A last name must be specified";
            } else if (strlen($lname) > 100) {
                $err_msgs['ct_last_name'] = "The last name must be no longer than 100 characters in length.";
            }
        }
        if (isset($_POST['ct_disp_name'])) {
            $dname = trim($_POST['ct_disp_name']);
            if (strlen($dname) > 200) {
                $err_msgs['ct_disp_name'] = "The display name must be no longer than 200 characters in length.";
            }
        }
    }

    //validate the address
    if (!isset($_POST['ad_type'])) {
        $err_msgs['ad_type'] = "An address type must be selected";
    } else if (isset($_POST['ad_type'])) {
        $type = trim($_POST['ad_type']);
        if (!(($type == "Home") || ($type == "Work") || ($type == "Other"))) {
            $err_msgs['ad_type'] = "A valid address type must be selected";
        }
    }
    if (!isset($_POST['ad_line_1'])) {
        $err_msgs['ad_line_1'] = "The first address line must not be empty";
    } else {
        $line1 = trim($_POST['ad_line_1']);
        if (strlen($line1) == 0) {
            $err_msgs['ad_line_1'] = "The first address line must not be empty";
        } else if (strlen($line1) > 100) {
            $err_msgs['ad_line_1'] = "The first address line is too long";
        }
    }
    if (isset($_POST['ad_line_2'])) {
        $line2 = trim($_POST['ad_line_2']);
        if (strlen($line2) > 100) {
            $err_msgs[] = "The second address line is too long";
        }
    }

    if (isset($_POST['ad_line_3'])) {
        $line3 = trim($_POST['ad_line_3']);
        if (strlen($line3) > 100) {
            $err_msgs['ad_line_3'] = "The third address line is too long";
        }
    }

    if (!isset($_POST['ad_city'])) {
        $err_msgs['ad_city'] = "A city name must be entered";
    } else {
        $city = trim($_POST['ad_city']);
        if (strlen($city) == 0) {
            $err_msgs['ad_city'] = "A city name must be entered";
        } else if (strlen($city) > 50) {
            $err_msgs['ad_city'] = "The city name is too long";
        }
    }

    if (isset($_POST['ad_province'])) {
        $prov = trim($_POST['ad_province']);
        if (strlen($prov) > 50) {
            $err_msgs['ad_province'] = "The province field is too long";
        }
    }

    if (isset($_POST['ad_post_code'])) {
        $post = trim($_POST['ad_post_code']);
        if (strlen($post) > 15) {
            $err_msgs['ad_post_code'] = "The post code field is too long";
        }
    }

    if (isset($_POST['ad_country'])) {
        $country = trim($_POST['ad_country']);
        if (strlen($country) > 50) {
            $err_msgs['ad_country'] = "The country field is too long";
        }
    }

    //validate the phone
    if (!isset($_POST['ph_type'])) {
        $err_msgs['ph_type'] = "An phone number type must be selected";
    } else if (isset($_POST['ph_type'])) {
        $type = trim($_POST['ph_type']);
        if (!(($type == "Home") || ($type == "Work") || ($type == "Mobile")
            || ($type == "Fax") || ($type == "Other"))) {
            $err_msgs['ph_type'] = "An phone number type must be selected";
        }
    }
    if (!isset($_POST['ph_number'])) {
        $err_msgs['ph_number'] = "The phone number field must not be empty";
    } else {
        $number = trim($_POST['ph_number']);
        if (strlen($number) == 0) {
            $err_msgs['ph_number'] = "The phone number field must not be empty";
        } else if (strlen($number) > 50) {
            $err_msgs['ph_number'] = "The phone number is too long";
        } else if (!preg_match("/^(((\+1-)?[2-9]\d{2}-)|(1?\([2-9]\d{2}\)))[2-9]\d{2}-\d{4}$/", $number)) {
            $err_msgs['ph_number'] = "The phone number format is not correct";
        }
    }

    //validate the email
    if (!isset($_POST['em_type'])) {
        $err_msgs['em_type'] = "An email type must be selected";
    } else if (isset($_POST['em_type'])) {
        $type = trim($_POST['em_type']);
        if (!(($type == "Home") || ($type == "Work")
            || ($type == "Other"))) {
            $err_msgs['em_type'] = "An email type must be selected";
        }
    }
    if (!isset($_POST['em_email'])) {
        $err_msgs['em_email'] = "The email address field must not be empty";
    } else {
        $email = trim($_POST['em_email']);
        if (strlen($email) == 0) {
            $err_msgs['em_email'] = "The email address field must not be empty";
        } else if (strlen($email) > 255) {
            $err_msgs['em_email'] = "The email address is too long";
        } else if (!preg_match("/^([a-zA-Z0-9][a-zA-Z0-9_\-\.]*)@([a-zA-Z0-9][a-zA-Z0-9\-\.]*)$/", $email)) {
            $err_msgs['em_email'] = "The Email field format is not correct";
        }
    }

    //validate the contact web
    if (!isset($_POST['we_type'])) {
        $err_msgs['we_type'] = "A web site type must be selected";
    } else if (isset($_POST['we_type'])) {
        $type = trim($_POST['we_type']);
        if (!(($type == "Personal") || ($type == "Work")
            || ($type == "LinedIn")
            || ($type == "Facebook") || ($type == "Other"))) {
            $err_msgs['we_type'] = "A web site type must be selected";
        }
    }
    if (!isset($_POST['we_url'])) {
        $err_msgs['we_url'] = "The URL field must not be empty";
    } else {
        $url = trim($_POST['we_url']);
        if (strlen($url) == 0) {
            $err_msgs['we_url'] = "The URL field must not be empty";
        } else if (strlen($url) > 255) {
            $err_msgs['we_url'] = "The URL is too long";
        }
    }

    //validate the note
    if (!isset($_POST['no_note'])) {
        $err_msgs['no_note'] = "The note field must not be empty";
    } else {
        $note = trim($_POST['no_note']);
        if (strlen($note) == 0) {
            $err_msgs['no_note'] = "The note field must not be empty";
        } else if (strlen($note) > 65530) {
            $err_msgs['no_note'] = "The note is too long";
        }
    }
    return $err_msgs;
}

?>