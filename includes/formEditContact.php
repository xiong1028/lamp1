<?php
/**
 * Purpose: a function to present the data and allow the user to modify their data
 * Authors: Xiong Li, Haiyun Xiao, Kunj Bhavsar,Nayan, Goswami
 *
 **/

function formEditContact($selectedId, $err_msgs)
{
    $db_conn = dbconnect('localhost', 'week7', 'lamp1user', '!Lamp1!');
    ?>
    <h3>Contact Edit</h3>
    <br>
    <form method="post">
        <table>
            <?php
            formContactName_Edit($db_conn, $selectedId, $err_msgs);
            formContactAddress_Edit($db_conn, $selectedId, $err_msgs);
            formContactPhone_Edit($db_conn, $selectedId, $err_msgs);
            formContactEmail_Edit($db_conn, $selectedId, $err_msgs);
            formContactWebSite_Edit($db_conn, $selectedId, $err_msgs);
            formContactNote_Edit($db_conn, $selectedId, $err_msgs);
            ?>
            <tr>
                <td><input type="submit" name="ct_b_cancel" value="Cancel"></td>
                <td><input type="submit" name="ct_b_confirm" value="Confirm"></td>
            </tr>
        </table>
    </form>

    <?php
//    dbdisconnect($db_conn);
}

?>

<?php
//edit the contact name form
function formContactName_Edit($db_conn, $selectedId, $err_msgs)
{
    $qry = "select ct_id,ct_type,ct_first_name,ct_last_name,ct_disp_name from contact where ct_id ='" . $selectedId . "';";
    $ct_id ="";
    $ct_type = "";
    $fname = "";
    $lname = "";
    $ct_disp_name = "";

    if ((isset($_POST['ct_b_confirm'])) && ($_POST['ct_b_confirm'] == "Confirm")) {
        $ct_id = $_POST['ct_id'];
        $ct_type = $_POST['ct_type'];
        $fname = $_POST['ct_first_name'];
        $lname = $_POST['ct_last_name'];
        $ct_disp_name = $_POST['ct_disp_name'];
    } else if ($rs = $db_conn->query($qry)) {
        if ($rs->num_rows > 0) {
            $row = $rs->fetch_assoc();
            $ct_id = $row['ct_id'];
            $ct_type = $row['ct_type'];
            $fname = $row['ct_first_name'];
            $lname = $row['ct_last_name'];
            $ct_disp_name = $row['ct_disp_name'];
        }
    }
    ?>
    <tr>
        <td>No #</td>
        <td><input type="text" name="ct_id" id="ct_id"  value="<?php echo $ct_id;?>" readonly="readonly">
        </td>
    </tr>
    <tr>
        <td><label for="ct_type">Contact Type</label></td>
        <td>
            <select id="ct_type" name="ct_type" size="1">
                <option value="Choice" <?php if (strlen($ct_type) == 0) echo "selected"; ?>>Select type
                </option>
                <option value="Family" <?php if ($ct_type == "Family") echo "selected"; ?>>Family
                </option>
                <option value="Friend" <?php if ($ct_type == "Friend") echo "selected"; ?>>Friend
                </option>
                <option value="Business" <?php if ($ct_type == "Business") echo "selected"; ?> >Business
                </option>
                <option value="Other" <?php if ($ct_type == "Other") echo "selected"; ?>>Other
                </option>
            </select>
        </td>
        <td>
            <span style="color: red;margin-left: 15px;">
            <?php
            if (isset($err_msgs['ct_type'])) {
                echo $err_msgs['ct_type'];
            } else {
                echo "";
            }
            ?>
            </span>
        </td>
    </tr>

    <tr>
        <td><label for="ct_first_name">First Name</label></td>
        <td><input type="text" name="ct_first_name" id="ct_first_name" size="50" maxlength="100"
                   value="<?php echo $fname; ?>"></td>
        <td>
            <span style="color: red;margin-left: 15px;">
            <?php
            if (isset($err_msgs['ct_first_name'])) {
                echo $err_msgs['ct_first_name'];
            } else {
                echo "";
            }
            ?>
            </span>
        </td>
    </tr>
    <tr>
        <td><label for="ct_last_name">Last Name</label></td>
        <td><input type="text" name="ct_last_name" id="ct_last_name" size="50" maxlength="100"
                   value="<?php echo $lname; ?>"></td>
        <td>
            <span style="color: red;margin-left: 15px;">
            <?php
            if (isset($err_msgs['ct_last_name'])) {
                echo $err_msgs['ct_last_name'];
            } else {
                echo "";
            }
            ?>
            </span>
        </td>
    </tr>
    <tr>
        <td><label for="ct_disp_name">Display Name</label></td>
        <td><input type="text" name="ct_disp_name" id="ct_disp_name" size="50" maxlength="200"
                   value="<?php echo $ct_disp_name; ?>"></td>
        <td>
            <span style="color: red;margin-left: 15px;">
            <?php
            if (isset($err_msgs['ct_disp_name'])) {
                echo $err_msgs['ct_disp_name'];
            } else {
                echo "";
            }
            ?>
            </span>
        </td>
    </tr>
    <?php
}

?>
<?php
//edit the contact Address form
function formContactAddress_Edit($db_conn, $selectedID, $err_msgs)
{
    $qry = "SELECT ad_type,ad_line_1,ad_line_2,ad_line_3,ad_city,ad_province,ad_post_code,ad_country FROM contact_address where ad_ct_id = '" . $selectedID . "';";
    $ad_type = "";
    $ad_line_1 = "";
    $ad_line_2 = "";
    $ad_line_3 = "";
    $ad_city = "";
    $ad_province = "";
    $ad_post_code = "";
    $ad_country = "";

    if ((isset($_POST['ct_b_confirm'])) && ($_POST['ct_b_confirm'] == "Confirm")) {
        $ad_type = $_POST['ad_type'];
        $ad_line_1 = $_POST['ad_line_1'];
        $ad_line_2 = $_POST['ad_line_2'];
        $ad_line_3 = $_POST['ad_line_3'];
        $ad_city = $_POST['ad_city'];
        $ad_province = $_POST['ad_province'];
        $ad_post_code = $_POST['ad_post_code'];
        $ad_country = $_POST['ad_country'];
    } else if ($rs = $db_conn->query($qry)) {
        if ($rs->num_rows > 0) {
            $row = $rs->fetch_assoc();
            $ad_type = $row['ad_type'];
            $ad_line_1 = $row['ad_line_1'];
            $ad_line_2 = $row['ad_line_2'];
            $ad_line_3 = $row['ad_line_3'];
            $ad_city = $row['ad_city'];
            $ad_province = $row['ad_province'];
            $ad_post_code = $row['ad_post_code'];
            $ad_country = $row['ad_country'];
        }
    }
    ?>
    <tr>
        <td><label for="ad_type">Address Type</label></td>
        <td>
            <select id="ad_type" name="ad_type" size="1">
                <option value="Choice" <?php if (strlen($ad_type) == 0) echo "selected"; ?>>Choose type
                </option>
                <option value="Home" <?php if ($ad_type == "Home") echo "selected"; ?>>Home
                </option>
                <option value="Work" <?php if ($ad_type == "Work") echo "selected"; ?>>Work
                </option>
                <option value="Other" <?php if ($ad_type == "Other") echo "selected"; ?>>Other
                </option>
            </select>
        </td>
        <td>
            <span style="color: red;margin-left: 15px;">
            <?php
            if (isset($err_msgs['ad_type'])) {
                echo $err_msgs['ad_type'];
            } else {
                echo "";
            }
            ?>
            </span>
        </td>
    </tr>
    <tr>
        <td><label for="ad_line_1">Address Line 1</label></td>
        <td><input type="text" name="ad_line_1" id="ad_line_1" size="50"
                   maxlength="100" value="<?php echo $ad_line_1; ?>"></td>
        <td>
            <span style="color: red;margin-left: 15px;">
            <?php
            if (isset($err_msgs['ad_line_1'])) {
                echo $err_msgs['ad_line_1'];
            } else {
                echo "";
            }
            ?>
            </span>
        </td>
    </tr>
    <tr>
        <td><label for="ad_line_2">Address Line 2</label></td>
        <td><input type="text" name="ad_line_2" id="ad_line_2" size="50"
                   maxlength="100" value="<?php echo $ad_line_2; ?>"></td>
        <td>
            <span style="color: red;margin-left: 15px;">
            <?php
            if (isset($err_msgs['ad_line_2'])) {
                echo $err_msgs['ad_line_2'];
            } else {
                echo "";
            }
            ?>
            </span>
        </td>
    </tr>
    <tr>
        <td><label for="ad_line_3">Address Line 3</label></td>
        <td><input type="text" name="ad_line_3" id="ad_line_3" size="50"
                   maxlength="100" value="<?php echo $ad_line_3; ?>"></td>
        <td>
            <span style="color: red;margin-left: 15px;">
            <?php
            if (isset($err_msgs['ad_line_3'])) {
                echo $err_msgs['ad_line_3'];
            } else {
                echo "";
            }
            ?>
            </span>
        </td>
    </tr>
    <tr>
        <td><label for="add_city">City</label></td>
        <td><input type="text" name="ad_city" id="ad_city" size="30"
                   maxlength="50" value="<?php echo $ad_city; ?>"></td>
        <td>
            <span style="color: red;margin-left: 15px;">
            <?php
            if (isset($err_msgs['ad_city'])) {
                echo $err_msgs['ad_city'];
            } else {
                echo "";
            }
            ?>
            </span>
        </td>

    </tr>
    <tr>
        <td><label for="ad_province">Province</label></td>
        <td><input type="text" name="ad_province" id="ad_province" size="30"
                   maxlength="50" value="<?php echo $ad_province; ?>"></td>
        <td>
            <span style="color: red;margin-left: 15px;">
            <?php
            if (isset($err_msgs['ad_province'])) {
                echo $err_msgs['ad_province'];
            } else {
                echo "";
            }
            ?>
            </span>
        </td>
    </tr>
    <tr>
        <td><label for="ad_post_code">Post Code</label></td>
        <td><input type="text" name="ad_post_code" id="ad_post_code" size="30"
                   maxlength="15" value="<?php echo $ad_post_code; ?>"></td>
        <td>
            <span style="color: red;margin-left: 15px;">
            <?php
            if (isset($err_msgs['ad_post_code'])) {
                echo $err_msgs['ad_post_code'];
            } else {
                echo "";
            }
            ?>
            </span>
        </td>
    </tr>
    <tr>
        <td><label for="ad_country">County</label></td>
        <td><input type="text" name="ad_country" id="ad_country" size="30"
                   maxlength="50" value="<?php echo $ad_country; ?>"></td>
        <td>
            <span style="color: red;margin-left: 15px;">
            <?php
            if (isset($err_msgs['ad_country'])) {
                echo $err_msgs['ad_country'];
            } else {
                echo "";
            }
            ?>
            </span>
        </td>
    </tr>
    <?php
}

?>


<?php
//edit the contact Phone form
function formContactPhone_Edit($db_conn, $selectedID, $err_msgs)
{
    $qry = "SELECT ph_type, ph_number FROM contact_phone where ph_ct_id = '" . $selectedID . "';";
    $ph_type = "";
    $ph_number = "";

    if ((isset($_POST['ct_b_confirm'])) && ($_POST['ct_b_confirm'] == "Confirm")) {
        $ph_type = $_POST['ph_type'];
        $ph_number = $_POST['ph_number'];
    } else if ($rs = $db_conn->query($qry)) {
        if ($rs->num_rows > 0) {
            $row = $rs->fetch_assoc();
            $ph_type = $row['ph_type'];
            $ph_number = $row['ph_number'];
        }
    }
    ?>

    <tr>
        <td><label for="ph_type">Phone# Type</label></td>
        <td>
            <select id="ph_type" name="ph_type" size="1">
                <option value="Choice" <?php if (strlen($ph_type) == 0) echo "selected"; ?>>Choose type
                </option>
                <option value="Home" <?php if ($ph_type == "Home") echo "selected"; ?>>Home
                </option>
                <option value="Work" <?php if ($ph_type == "Work") echo "selected"; ?>>Work
                </option>
                <option value="Mobile" <?php if ($ph_type == "Mobile") echo "selected"; ?>>Mobile
                </option>
                <option value="Fax" <?php if ($ph_type == "Fax") echo "selected"; ?>>Fax
                </option>
                <option value="Other" <?php if ($ph_type == "Other") echo "selected"; ?>>Other
                </option>
            </select>
        </td>
        <td>
            <span style="color: red;margin-left: 15px;">
            <?php
            if (isset($err_msgs['ph_type'])) {
                echo $err_msgs['ph_type'];
            } else {
                echo "";
            }
            ?>
            </span>
        </td>
    </tr>
    <tr>
        <td><label for="ph_number">Phone Number</label></td>
        <td><input type="text" name="ph_number" id="ph_number" size="30"
                   maxlength="50" value="<?php echo $ph_number; ?>">
        </td>
        <td>
            <span style="color: red;margin-left: 15px;">
            <?php
            if (isset($err_msgs['ph_number'])) {
                echo $err_msgs['ph_number'];
            } else {
                echo "";
            }
            ?>
            </span>
        </td>
    </tr>
    <?php
}

?>

<?php
//edit the contact email form
function formContactEmail_Edit($db_conn, $selectedID,$err_msgs)
{
    $qry = "SELECT em_type,em_email FROM contact_email where em_ct_id = '" . $selectedID . "';";
    $em_type = "";
    $em_email = "";


    if ((isset($_POST['ct_b_confirm'])) && ($_POST['ct_b_confirm'] == "Confirm")) {
        $em_type = $_POST['em_type'];
        $em_email = $_POST['em_email'];
    } else if ($rs = $db_conn->query($qry)) {
        if ($rs->num_rows > 0) {
            $row = $rs->fetch_assoc();
            $em_type = $row['em_type'];
            $em_email = $row['em_email'];
        }
    }
    ?>
    <tr>
        <td><label for="em_type">Email Type</label></td>
        <td>
            <select id="em_type" name="em_type" size="1">
                <option value="Choice" <?php if (strlen($em_type) == 0) echo "selected"; ?>>Choose type
                </option>
                <option value="Home" <?php if ($em_type == "Home") echo "selected"; ?>>Home
                </option>
                <option value="Work" <?php if ($em_type == "Work") echo "selected"; ?>>Work
                </option>
                <option value="Other" <?php if ($em_type == "Other") echo "selected"; ?>>Other
                </option>
            </select>
        </td>
        <td>
            <span style="color: red;margin-left: 15px;">
            <?php
            if (isset($err_msgs['em_type'])) {
                echo $err_msgs['em_type'];
            } else {
                echo "";
            }
            ?>
            </span>
        </td>
    </tr>
    <tr>
        <td><label for="em_email">Email Address</label></td>
        <td><input type="text" name="em_email" id="em_email" size="50"
                   maxlength="255" value="<?php echo $em_email; ?>"></td>
        <td>
            <span style="color: red;margin-left: 15px;">
            <?php
            if (isset($err_msgs['em_email'])) {
                echo $err_msgs['em_email'];
            } else {
                echo "";
            }
            ?>
            </span>
        </td>
    </tr>


    <?php
}

?>

<?php
//edit the contact_Web_Site form
function formContactWebSite_Edit($db_conn, $selectedID,$err_msgs)
{
    $qry = "SELECT we_type,we_url FROM contact_web where we_ct_id = '" . $selectedID . "';";
    $we_type = "";
    $we_url = "";

    if ((isset($_POST['ct_b_confirm'])) && ($_POST['ct_b_confirm'] == "Confirm")) {
        $we_type = $_POST['we_type'];
        $we_url = $_POST['we_url'];
    } else if ($rs = $db_conn->query($qry)) {
        if ($rs->num_rows > 0) {
            $row = $rs->fetch_assoc();
            $we_type = $row['we_type'];
            $we_url = $row['we_url'];
        }
    } ?>
    <tr>
        <td><label for="web_type">Web Site Type</label></td>
        <td>
            <select id="we_type" name="we_type" size="1">
                <option value="Choice" <?php if (strlen($we_type) == 0) echo "selected"; ?>>Choose type
                </option>
                <option value="Personal" <?php if ($we_type == "Personal") echo "selected"; ?>>Personal
                </option>
                <option value="Work" <?php if ($we_type == "Work") echo "selected"; ?>>Work
                </option>
                <option value="Linkedln" <?php if ($we_type == "Linkedln") echo "selected"; ?>>Linkedln
                </option>
                <option value="Facebook" <?php if ($we_type == "Facebook") echo "selected"; ?>>Facebook
                </option>
                <option value="Other" <?php if ($we_type == "Other") echo "selected"; ?>>Other
                </option>
            </select>
        </td>
        <td>
            <span style="color: red;margin-left: 15px;">
            <?php
            if (isset($err_msgs['we_type'])) {
                echo $err_msgs['we_type'];
            } else {
                echo "";
            }
            ?>
            </span>
        </td>
    </tr>
    <tr>
        <td><label for="we_url">Web Site URL</label></td>
        <td><input type="text" name="we_url" id="we_url" size="50"
                   maxlength="255" value="<?php echo $we_url; ?>"></td>
        <td>
            <span style="color: red;margin-left: 15px;">
            <?php
            if (isset($err_msgs['we_url'])) {
                echo $err_msgs['we_url'];
            } else {
                echo "";
            }
            ?>
            </span>
        </td>
    </tr>
    <?php
}

?>

<?php
//edit the contact Note form
function formContactNote_Edit($db_conn, $selectedID,$err_msgs)
{
    $qry = "SELECT no_type,no_note FROM contact_note where no_ct_id = '" . $selectedID . "';";
    $no_type = "";
    $no_note = "";

    if ((isset($_POST['ct_b_confirm'])) && ($_POST['ct_b_confirm'] == "Confirm")) {
        $no_type = "";
        $no_note = $_POST['no_note'];
    } else if ($rs = $db_conn->query($qry)) {
        if ($rs->num_rows > 0) {
            $row = $rs->fetch_assoc();
            $no_type = $row['no_type'];
            $no_note = $row['no_note'];
        }
    } ?>

    <tr>
        <td><label for="no_note">Note</label></td>
        <td><textarea id="no_note" name="no_note" rows="10" cols="50"
                      maxlength="65530"><?php echo $no_note; ?></textarea></td>
        <td>
            <span style="color: red;margin-left: 15px;">
            <?php
            if (isset($err_msgs['no_note'])) {
                echo $err_msgs['no_note'];
            } else {
                echo "";
            }
            ?>
            </span>
        </td>
    </tr>
    <?php
}

?>


