<?php
/**
 * Purpose: Build a function back to index.php when user did not select any record
 * Authors: Xiong Li, Haiyun Xiao, Kunj Bhavsar,Nayan, Goswami
 *
 **/
function formBackToIndex()
{
    ?>
    <form method="post">
        <h4>No record is chosen. Please select the record firstly...</h4>
        <br>
        <input type="submit" name="ct_b_cancel" value="Cancel">
    </form>
    <?php
}

?>