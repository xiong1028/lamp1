<?php
/**
 * Purpose: build a function to delete the selected record
 * Authors: Xiong Li, Haiyun Xiao, Kunj Bhavsar,Nayan, Goswami
 *
 **/

function deleteRecord($selectedId)
{
    $db_conn = dbconnect('localhost', 'week7', 'lamp1user', '!Lamp1!');
    $qry_del= "delete from contact_address where ad_ct_id = ".$selectedId.";";
    $qry_del .= "delete from contact_phone where ph_ct_id = ".$selectedId.";";
    $qry_del .= "delete from contact_email where em_ct_id = ".$selectedId.";";
    $qry_del .= "delete from contact_web where we_ct_id = ".$selectedId.";";
    $qry_del .="delete from contact_note where no_ct_id= ".$selectedId.";";
    $qry_del .= "delete from contact where ct_id = ".$selectedId.";";

    $db_conn->multi_query($qry_del);
    dbdisconnect($db_conn);
}
?>


<?php
function confirmDeletePage()
{
    ?>
    <form method="POST">
        <h3>Delete Record</h3>
        <p>Are you sure to delete selected record? </p>
        <input type="submit" name="ct_b_cancel" value="Cancel">
        <input type="submit" name="ct_b_confirm" value="Confirm">
    </form>
    <?php
}

?>

<?php
function delContactAddress($db_conn,$selectedId){
    $qry = "delete from contact_address where ad_ct_id = ".$selectedId.";";
    $db_conn->query($qry);
}
?>


