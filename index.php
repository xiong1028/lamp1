<?php
session_start();
if (!isset($_SESSION['mode'])) {
    $_SESSION['mode'] = "Display";
}
require_once("./includes/db_operations.php");
require_once("./includes/displayContacts.php");
require_once("./includes/formContactType.php");
require_once("./includes/formContactName.php");
require_once("./includes/formContactAddress.php");
require_once("./includes/formContactPhone.php");
require_once("./includes/formContactEmail.php");
require_once("./includes/formContactWeb.php");
require_once("./includes/formContactNote.php");
require_once("./includes/formContactSave.php");
require_once("./includes/clearAddContactFromSession.php");
require_once("./includes/displayErrors.php");
require_once("./includes/formContactDisplay.php");
require_once("./includes/formEditContact.php");
require_once("./includes/formBackToIndex.php");
require_once("./includes/validateReEditForm.php");
require_once("./includes/reEditSession.php");
require_once("./includes/reEditDataUpdate.php");
require_once("./includes/deleteRecord.php");
require_once ("./includes/displayRecord.php");

?>
<html>
<head>
    <title>Contact List</title>
</head>
<body>
<?php
if (isset($_POST['ct_b_add']) && ($_POST['ct_b_add'] == "Add New Contact")) {
    $_SESSION['mode'] = "Add";
    $_SESSION['add_part'] = 0;
} else if (isset($_POST['ct_b_edit']) && ($_POST['ct_b_edit'] == "Edit")) {
    $_SESSION['mode'] = "Edit";
} else if (isset($_POST['ct_b_delete']) && ($_POST['ct_b_delete'] == "Delete")) {
    $_SESSION['mode'] = "Delete";
} else if (isset($_POST['ct_b_view']) && ($_POST['ct_b_view'] == "View Details")) {
    $_SESSION['mode'] = "View";
} else if (isset($_POST['ct_b_cancel']) && ($_POST['ct_b_cancel'] == "Cancel")) {
    if ($_SESSION['mode'] == "Add") {
        $_SESSION['add_part'] = 0;
        //$_SESSION['add_part'] = 0;
        clearAddContactFromSession();
    }
    $_SESSION['mode'] = "Display";
}

//The route of the whole project goes Here
if (($_SESSION['mode'] == "Add") && ($_SERVER['REQUEST_METHOD'] == "GET")) {
    switch ($_SESSION['add_part']) {
        case 0:
        case 1:
            formContactType();
            break;
        case 2:
            formContactName();
            break;
        case 3:
            formContactAddress();
            break;
        case 4:
            formContactPhone();
            break;
        case 5:
            formContactEmail();
            break;
        default:
    }
} else if ($_SESSION['mode'] == "Add") {
    switch ($_SESSION['add_part']) {
        case 0:
            echo "<h1> Add New Contact </h1>\n";
            $_SESSION['add_part'] = 1;
            formContactType();
            break;
        case 1:
            echo "<h1> Add New Contact </h1>\n";
            $err_msgs = validateContactType();
            if (count($err_msgs) > 0) {
                displayErrors($err_msgs);
                formContactType();
            } else {
                contactTypePostToSession();
                $_SESSION['add_part'] = 2;
                formContactName();
            }
            break;
        case 2:
            echo "<h1> Add New Contact </h1>\n";
            $err_msgs = validateContactName();
            if (count($err_msgs) > 0) {
                displayErrors($err_msgs);
                formContactName();
            } else if ((isset($_POST['ct_b_next']))
                && ($_POST['ct_b_next'] == "Next")) {
                contactNamePostToSession();
                $_SESSION['add_part'] = 3;
                formContactAddress();
            } else if ((isset($_POST['ct_b_back']))
                && ($_POST['ct_b_back'] == "Back")) {
                contactNamePostToSession();
                $_SESSION['add_part'] = 1;
                formContactType();
            }
            break;
        case 3:
            echo "<h1> Add New Contact </h1>\n";
            $err_msgs = validateContactAddress();
            if ((!isset($_POST['ct_b_skip'])) && (count($err_msgs) > 0)) {
                displayErrors($err_msgs);
                formContactAddress();
            } else if (isset($_POST['ct_b_skip'])) {
                $_SESSION['add_part'] = 4;
                formContactPhone();
            } else if ((isset($_POST['ct_b_next']))
                && ($_POST['ct_b_next'] == "Next")) {
                contactAddressPostToSession();
                $_SESSION['add_part'] = 4;
                formContactPhone();
            } else if ((isset($_POST['ct_b_back']))
                && ($_POST['ct_b_back'] == "Back")) {
                contactAddressPostToSession();
                $_SESSION['add_part'] = 2;
                formContactName();
            }
            break;
        case 4:
            echo "<h1> Add New Contact </h1>\n";
            $err_msgs = validateContactPhone();
            if ((!isset($_POST['ct_b_skip'])) && (count($err_msgs) > 0)) {
                displayErrors($err_msgs);
                formContactPhone();
            } else if (isset($_POST['ct_b_skip'])) {
                $_SESSION['add_part'] = 5;
                formContactEmail();
            } else if ((isset($_POST['ct_b_next']))
                && ($_POST['ct_b_next'] == "Next")) {
                contactPhonePostToSession();
                $_SESSION['add_part'] = 5;
                formContactEmail();
            } else if ((isset($_POST['ct_b_back']))
                && ($_POST['ct_b_back'] == "Back")) {
                contactPhonePostToSession();
                $_SESSION['add_part'] = 3;
                formContactAddress();
            }
            break;
        case 5:
            echo "<h1> Add New Contact </h1>\n";
            $err_msgs = validateContactEmail();
            if ((!isset($_POST['ct_b_skip'])) && (count($err_msgs) > 0)) {
                displayErrors($err_msgs);
                formContactEmail();
            } else if (isset($_POST['ct_b_skip'])) {
                $_SESSION['add_part'] = 6;
                formContactWeb();
            } else if ((isset($_POST['ct_b_next']))
                && ($_POST['ct_b_next'] == "Next")) {
                contactEmailPostToSession();
                $_SESSION['add_part'] = 6;
                formContactWeb();
            } else if ((isset($_POST['ct_b_back']))
                && ($_POST['ct_b_back'] == "Back")) {
                contactEmailPostToSession();
                $_SESSION['add_part'] = 4;
                formContactPhone();
            }
            break;
        case 6:
            echo "<h1> Add New Contact </h1>\n";
            $err_msgs = validateContactWeb();
            if ((!isset($_POST['ct_b_skip'])) && (count($err_msgs) > 0)) {
                displayErrors($err_msgs);
                formContactWeb();
            } else if (isset($_POST['ct_b_skip'])) {
                $_SESSION['add_part'] = 7;
                formContactNote();
            } else if ((isset($_POST['ct_b_next']))
                && ($_POST['ct_b_next'] == "Next")) {
                contactWebPostToSession();
                $_SESSION['add_part'] = 7;
                formContactNote();
            } else if ((isset($_POST['ct_b_back']))
                && ($_POST['ct_b_back'] == "Back")) {
                contactWebPostToSession();
                $_SESSION['add_part'] = 5;
                formContactEmail();
            }
            break;
        case 7:
            echo "<h1> Add New Contact </h1>\n";
            $err_msgs = validateContactNote();
            if ((!isset($_POST['ct_b_skip'])) && (count($err_msgs) > 0)) {
                displayErrors($err_msgs);
                formContactNote();
            } else if (isset($_POST['ct_b_skip'])) {
                $_SESSION['add_part'] = 8;
                formContactSave();
            } else if ((isset($_POST['ct_b_next']))
                && ($_POST['ct_b_next'] == "Next")) {
                contactNotePostToSession();
                $_SESSION['add_part'] = 8;
                formContactSave();
            } else if ((isset($_POST['ct_b_back']))
                && ($_POST['ct_b_back'] == "Back")) {
                contactNotePostToSession();
                $_SESSION['add_part'] = 6;
                formContactWeb();
            }
            break;
        case 8:
            if ((isset($_POST['ct_b_next']))
                && ($_POST['ct_b_next'] == "Save")) {
                $db_conn = dbconnect('localhost', 'week7', 'lamp1user', '!Lamp1!');
                saveContact($db_conn);
                dbdisconnect($db_conn);
                $_SESSION['add_part'] = 0;
                clearAddContactFromSession();
                $_SESSION['mode'] = "Display";
                formContactDisplay();
            } else if ((isset($_POST['ct_b_back']))
                && ($_POST['ct_b_back'] == "Back")) {
                echo "<h1> Add New Contact </h1>\n";
                $_SESSION['add_part'] = 7;
                formContactNote();
            }
            break;
        default:
    }
} else if ($_SESSION['mode'] == "Edit") {
    if (isset($_POST['ct_id']) && isset($_POST['ct_b_confirm']) && ($_POST['ct_b_confirm'] == "Confirm")) {
        $errs_msgs = validateReEditForm();
        if (count($errs_msgs) > 0) {
            formEditContact($_POST['ct_id'], $errs_msgs);
        } else {
            reEditSession();
            reEditDataUpdate($_POST['ct_id']);
            $_SESSION['add_part'] = 0;
            clearAddContactFromSession();
            $_SESSION['mode'] = "Display";
            formContactDisplay();
        }
    } else if (isset($_POST['list_select'])) {
        formEditContact($_POST['list_select'][0], "");
    } else if ((isset($_POST['ct_b_cancel'])) && ($_POST['ct_b_cancel'] == "Cancel")) {
        if ($_SESSION['mode'] == "Add") {
            $_SESSION['add_part'] = 0;
            clearAddContactFromSession();
        }
        $_SESSION['mode'] = "Display";
        formContactDisplay();
    } else {
        formBackToIndex();
    }

} else if ($_SESSION['mode'] == "Delete") {
    if (isset($_POST['ct_b_confirm']) && ($_POST['ct_b_confirm'] == "Confirm")) {
        if (isset($_SESSION['selectedId'])) {
            deleteRecord($_SESSION['selectedId']);
            $_SESSION['add_part'] = 0;
            clearAddContactFromSession();
            $_SESSION['mode'] = "Display";
            formContactDisplay();
        }
    } else if (isset($_POST['list_select'])) {
        $_SESSION['selectedId'] = $_POST['list_select'][0];
        confirmDeletePage();

    } else if ((isset($_POST['ct_b_cancel'])) && ($_POST['ct_b_cancel'] == "Cancel")) {
        if ($_SESSION['mode'] == "Add") {
            $_SESSION['add_part'] = 0;
            clearAddContactFromSession();
        }
        $_SESSION['mode'] = "Display";
        formContactDisplay();
    } else {
        formBackToIndex();
    }
} else if ($_SESSION['mode'] == "View") {
    if (isset($_POST['ct_b_OK']) && ($_POST['ct_b_OK'] == "OK")) {
        if ($_SESSION['mode'] == "Add") {
            $_SESSION['add_part'] = 0;
            clearAddContactFromSession();
        }
        $_SESSION['mode'] = "Display";
        formContactDisplay();
    } else if (isset($_POST['list_select'])) {
        displayRecord($_POST['list_select'][0]);
    } else {
        formBackToIndex();
    }
} else if ($_SESSION['mode'] == "Display") {
    formContactDisplay();
}
?>
</body>
</html>

